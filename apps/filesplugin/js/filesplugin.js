OCA.Files.fileActions.registerAction({
    name: 'myDirectoryAction',
    displayName: t('my-app-id', 'Start signing'),
    mime: 'dir',
    permissions: OC.PERMISSION_READ,
    iconClass: 'icon-rename',
    actionHandler: async (name, context) => {
        if (context.fileInfoModel.attributes.mountType === "shared-root") {
            try {
                const response = await fetch('https://negociation-tool.aster-x.demo23.gxfs.fr/signature', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({"result": { idDocument: context.fileInfoModel.id, idUser: OC.currentUser, dir: context.dir, nameFolder: name }})
                });
                const responseData = await response.json();
                if(responseData.message){
                    OC.dialogs.info(responseData.message, 'Message');
                }else{
                    OC.dialogs.info('An error occurred while fetching data from API.', 'Error message');
                }
                
            } catch (error) {
                console.error('Fetch Error:', error);
                OC.dialogs.info('An error occurred while fetching data from API.', 'Error message');
            }
        } else {
            OC.dialogs.info('The file cannot be signed "' + name, 'Message');
        }
    },
});
