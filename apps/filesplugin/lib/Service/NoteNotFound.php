<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: yvan wilfried <ymbetene@yahoo.fr>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\FilesPlugin\Service;

class NoteNotFound extends \Exception {
}
