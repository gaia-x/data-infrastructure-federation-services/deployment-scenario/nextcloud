<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: yvan wilfried <ymbetene@yahoo.fr>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\FilesPlugin\Tests\Unit\Controller;

use OCA\FilesPlugin\Controller\NoteApiController;

class NoteApiControllerTest extends NoteControllerTest {
	public function setUp(): void {
		parent::setUp();
		$this->controller = new NoteApiController($this->request, $this->service, $this->userId);
	}
}
